package com.example.widget;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MyRadio extends AppCompatActivity {
    Button btn1;
    RadioGroup rg1;
    RadioButton rb1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_radio);

        rg1 = findViewById(R.id.radioGroup1);
        rg1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radio1) {
                    Toast.makeText(getApplicationContext(), "choice: Radio1",
                            Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.radio2) {
                    Toast.makeText(getApplicationContext(), "choice: Radio2",
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "choice: Radio3",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
        btn1 = findViewById(R.id.button1);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = rg1.getCheckedRadioButtonId();
                rb1 = (RadioButton) findViewById(selectedId);
                Toast.makeText(getApplicationContext(), rb1.getText() + " is selected",Toast.LENGTH_SHORT).show();
            }
        });
    }



    }

